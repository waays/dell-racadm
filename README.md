# dell-racadm

## Getting started

Just a easier access to `racadm` Dell tool.


## Build container

```
make build
```

## Use racadm

define this alias in your `~/.bash_aliases` file :
```
alias racadm='docker run -it --volume "$(pwd):/tmp" --user "$(stat -c%u:%g $HOME)" dell-racadm'
```

Current directory will be bound to "/tmp/" for easier file sharing with container.
