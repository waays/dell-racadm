FROM ubuntu:focal

RUN apt-get update && apt-get upgrade -y &&\
    apt-get install -y wget ca-certificates

# disable systemctl
RUN ln -s --force /usr/bin/true /usr/bin/systemctl

RUN wget -q https://linux.dell.com/repo/pgp_pubkeys/0x1285491434D8786F.asc -O /etc/apt/trusted.gpg.d/dell-archives.asc &&\
    echo 'deb http://linux.dell.com/repo/community/openmanage/10200/focal focal main' > /etc/apt/sources.list.d/dell.list

RUN apt-get update &&\
    apt-cache search idrac &&\
    apt-get install -y dmidecode libssl1.1 srvadmin-idracadm8

# racadm require "libssl.so" file...
RUN ln -s /usr/lib/x86_64-linux-gnu/libssl.so.1.1 /usr/lib/x86_64-linux-gnu/libssl.so

# debug helper
RUN find / -name 'idracadm*' -or -name 'racadm*'

ENTRYPOINT ["/usr/bin/racadm"]
